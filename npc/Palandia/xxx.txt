//Bitten man

//XXX-X.gat,XX,XX,0	script	xxx	XXX,{

	if(Evol_Quest == 3) goto Done;
	if(Evol_Quest == 2) goto Check;
	if(Evol_Quest == 1) goto Begin;

	npctalk "Welcome to Palandia "+ strcharinfo(0) +"!";
	mes "[xxx]";
	set Evol_Quest, 1;
	if (Sex)
        mes "\"Hello young traveler! This is the great town Palandia, I hope you like it!\"";
	if (!Sex)
        mes "\"Hello beautiful lady! This is the great town Palandia, I hope you like it!\"";
	next;

	set @TEMP,rand(4);

	if(@TEMP == 0) goto Topic1;
	if(@TEMP == 1) goto Topic2;
	if(@TEMP == 2) goto Topic3;
	if(@TEMP == 3) goto Topic4;

Begin:
	mes "[xxx]";
	if (Sex)
        mes "\"Hello young traveler! This is the great town Palandia, I hope you like it!\"";
	if (!Sex)
        mes "\"Hello beautiful lady! This is the great town Palandia, I hope you like it!\"";
	next;

	set @TEMP,rand(4);

	if(@TEMP == 0) goto Topic1;
	if(@TEMP == 1) goto Topic2;
	if(@TEMP == 2) goto Topic3;
	if(@TEMP == 3) goto Topic4;

Topic1:
	menu
		"Oh, I see that it's great! Wait, what's with your leg?", Leg,
        "Thanks for your kind welcome!", -;
	close;

Topic2:
	menu
        "Wow, cool! Hm, what's wrong with your leg?", Leg,
		"Great!", -;
	close;

Topic3:
	menu
        "Cool, it's so big! Wait, why is your left leg so strange?", Leg,
		"Thanks for welcome!", -;
	close;

Topic4:
	menu
		"I like it very much! Oh no there is some blood on your leg!", Leg,
		"Thanks for welcome!", -;
	close;

Leg:
	mes "[xxx]";
	mes "\"Oh, leg? I know, I was attacked by scorpions today, they are so aggressive!\"";
	next;
	menu
		"Can I help you?", Quest,
		"You should be more careful!", -;
	close;

Quest:
	mes "[xxx]";
	set Evol_Quest, 2;
    mes "\"Can you buy 1 Low Potion to cure this injury? Here is 30 GP for it and please don't spend it on anything else!\"";
	set zeny, zeny 30;
	next;
	goto Buy;

Buy:
	mes "[xxx]";
	mes "\"Just buy 1 Low Potion for me.\"";
	close;

Check:
	if (countitem("LowPotion") < 1) goto Buy;
	set Evol_Quest, 3;
	mes "[xxx]";
	mes "\"Thank you, now I can cure my injures and work again!\"";
	getexp 5, 1;
	delitem "LowPotion", 1;
	close;

Done:
	mes "[xxx]";
	mes "\"Mm-m, it was so tasty! Thanks!\"";
	close;
}