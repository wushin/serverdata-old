// Evol scripts.
// Authors:
//    Vasily_Makarov
//    4144
// Description:
//    npc to talking to

001-2-1.gat,57,25,0,1	script	Elmo	310,{
  mesn;
  mesq g(l("Hey! You saw? we've arrived on the port today, it was a long way from the other island...#0"), l("Hey! You saw? we've arrived to the port today, it was a long way from the other island...#1"));
  next;
  mesn;
  mesq l("I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.");
  next;
  mesn;
  mesq l("What will you do during this break?");
  next;
  mesn;
  mesq l("This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!");
  menu
    g(l("I wonder about that, I think I'll discover bit by bit and see what happens!#0"), l("I wonder about that, I think I'll discover bit by bit and see what happens!#1")), -,
    l("Oh really? I don't have much time right now, I need to go. Sorry."), l_NeedToGo;
  mesn;
  mesq l("Hahaha, I won't take much of your time, go ahead!");
  close;
l_NeedToGo:
  mesn;
  mesq l("Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!");
  close;
}
