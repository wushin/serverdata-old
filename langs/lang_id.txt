Copyright (C) 2010-2011  Evol Online
*hips*
*tersendak*

... By the power vested in me,
... Oleh kekuatan yang terpendam dalam diriku,

Ah, she *hips* is here.
Ah, dia *tersendak* ada disini.

Are you tired of being what you are?
Apa kamu lelah menjadi dirimu sendiri?

AreaNPC
AreaNPC

As you want!
Sesuai keinginanmu!

Ash brown
Abu cokelat

Bald
Botak

Bee spawned
Lebah bertelur

Billy Bons
Billy Bons

Black
Hitam

Blaf
Blaf

Ble *hips*... Imma great sail*hips*or!
Ble *tersendak*... Saya adalah seorang pelaut yang *tersendak* hebat!

Blonde
Pirang

Blue
Biru

Boots
Sepatu Boot

Bowl cut
Rambut mangkok

Brown
Cokelat

Brunette
Cokelat muda

Bun
Mengembang

Bunny ears
Telinga kelinci

Bye bye!
Dah!

Can you do something with my color?
Dapatkah kamu melakukan sesuatu dengan warnaku ?

Cap
Topi

Center parting/Short and slick
Belah tengah/Pendek dan licin

Choppy
Berombak

Combed back
Disisir ke belakang

Continue?
Lanjutkan?

Copper
Tembaga

Copper red
Merah tembaga

Could I ask you which language do you talk? Like that I add you on the list.
Dapatkah saya bertanya padamu bahasa apa yang kamu gunakan? Lalu saya akan menambahkanmu kedalam daftar.

Demon
Iblis

Desert hat
Topi padang pasir

Devis
Devis

Did you bring me @@ @@?#0
Apakah kamu membawakanku @@ @@?

Did you bring me @@ @@?#1
Apakah kamu membawakanku @@ @@?

Do you know some interesting people around here?
Apakah kamu tahu beberapa orang yang menarik di sekitar sini?

Do you want to create a bee?
Apa kamu ingin membuat seekor lebah?

Don't worry, I'm sure that everything will turn out well for the both of us!
Jangan khawatir, Saya yakin semua akan menjadi baik-baik saja untuk kita berdua!

Don't you see that I'm already talking?
Apa kamu mendengar apa yang telah saya bicarakan?

Done.
Selesai.

Dyeing failed. You lost dye item.
Pewarnaan gagal. Kamu kehilangan bahan pewarna.

Dyer
Tukang Pewarna

Elmo
Elmo

Emo
Emo

Enora
Enora

Eurni
Eurni

Eurni the Surgeon
Eurni si dokter bedah

Flat ponytail
Kuncir rata

For dye your item, you need white item and dye with required color.
Untuk mewarnai barang-barangmu, kamu membutuhkan barang yang berwarna putih dan pewarna sesuai yang diperlukan.

Fushia
Fushia

Ggrmm grmmm..
Grmm grmmm..

Gloves
Sarung tangan

Good job!
Kerja bagus!

Green
Hijau

Hahaha, I won't take much of your time, go ahead!
Hahaha, Aku tidak akan menghabiskan banyak waktumu, Pergi sekarang!

He takes a small list and starts reading it.
Dia mengambil daftar kecil dan mulai membacanya.

Headband
Ikat kepala

Hehe. Me, I will try to open a market, I've heard that it is a big city of commerce here, because of the port and all...
Hehe. Saya, akan mencoba membuka sebuah toko, Saya telah mendengar bahwa ini adalah kota dagang yang besar, karena pelabuhan dan yang lainnya...

Hello @@
Halo @@

Hello dear, what do you want today?#0
Hallo sayang, apa yang kamu butuhkan hari ini?

Hello dear, what do you want today?#1
Hallo sayang, apa yang kamu butuhkan hari ini

Hello, girl!
Halo, cewek!

Hello, stranger!
Halo, Orang asing!

Here is your reward!
Ini dia hadiahmu!

Hey! Yes, I'm already awake. So, it's end of our little road together, what will you do you now?
Hey! Ya, Saya sudah bangun. Jadi, ini adalah akhir dari perjalanan singkat kita, apa yang akan kamu lakukan sekarang ?

Hey! You saw? we've arrived on the port today, it was a long way from the other island...#0
Hey! Kamu melihatnya? hari ini kami telah sampai di pelabuhan, itu adalah sebuah perjalanan panjang dari pulau lain...

Hey! You saw? we've arrived to the port today, it was a long way from the other island...#1
Hey! Kamu melihatnya? hari ini kami telah sampai di pelabuhan, itu adalah sebuah perjalanan panjang dari pulau lain...

Hey, girl!
Hey, cewek!

Hey, guy!
Hei, bocah!

Hey, look at Julia, she's a beauty, isn't she? She's been looking at me for some minutes now... haha! Good luck, my friend.
Hey, lihat Julia, dia cantik bukan? Dia tertarik padaku untuk beberapa waktu ini... haha! Semoga beruntung, teman!

Hey, you lied to me.
Hey, Kamu berbohong kepadaku.

Hmm, I'm fine for now, thank you.
Hmm, Saya baik-baik saja sekarang, terima kasih.

How can I get it
Bagaimana cara agar saya bisa mendapatkannya

I can!
Saya bisa!

I don't need your help right now, come back later.
Saya tidak membutuhkan bantuanmu sekarang, datang lagi lain waktu.

I don't want change my language, sorry.
Maaf, Saya tidak ingin mengubah bahasa saya.

I gave you a reward. I can't give you more.
Saya telah memberimu hadiah. Saya tidak dapat memberimu lebih banyak lagi.

I made a mistake, I would like to change my language.#0
Saya membuat kesalahan, Saya berniat untuk mengubah bahasa saya.

I made a mistake, I would like to change my language.#1
Saya telah membuat kesalahan, Saya berniat untuk mengubah bahasa yang saya gunakan

I need help for cleaning the wedge of the ship, but you aren't strong enough to help me.
Saya membutuhkan batuan untuk membersihkan pasak kapal, tetapi kamu belum cukup kuat untuk membantu saya.

I need somebody who can clean the bottom of the ship, can you help me?
Saya membutuhkan seseorang yang dapat membersihkan bagian bawah kapal, dapatkah kamu mebantuku?

I sentence you to death... twice.
Saya memutuskan kamu untuk mati... dua kali.

I speak Dutch
Saya berbicara bahasa Belanda

I speak English
Saya berbicara bahasa Inggris

I speak Flemish
Saya berbicara bahasa Belanda-Flemish

I speak French
Saya berbicara bahasa Perancis

I speak Greek


I speak Indonesian


I speak Italian
Saya berbicara bahasa Italia

I speak Russian
Saya berbicara bahasa Russia

I speak Spanish
Saya berbicara bahasa Spanyol

I think that I will explore around a bit and see what I can find, and you?
Saya rasa, saya akan pergi menjelajah sebentar dan melihat apa yang dapat saya temukan, kalau kamu?

I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.
Saya rasa kapal ini akan tetap disini untuk beberapa lama, kita membutuhkan banyak makanan dan kapal kita membutuhkan beberapa perbaikan sebelum keembali ke lautan.

I think you can help me, right?
Saya rasa kamu dapat membatuku, benar?

I want to be a Human
Saya ingin menjadi manusia

I want to be a Ifitron
Saya ingin menjadi seorang Ifitrton

I want to be a Orc
Saya mau menjadi seorang Orc

I want to be a Sparron
Saya ngin menjadi seorang Sparron

I want to be a Ukar
Saya ingin menjadi seorang Ukar

I want to dye to different color
Saya ingin memberi warna yang berbeda

I will give you @@gp.
Saya akan memberimu @@gp

I will need some coffee...
Saya akan membutuhkan kopi...

I will rest a bit here or in the town, I don't feel so well after this trip...
Saya akan beristirahat sebentar disini atau di kota, Saya merasa tidak enak badan setelah perjalanan ini...

I will!
Saya akan melakukannya!

I wonder about that, I think I'll discover bit by bit and see what happens!#0
Saya ragu tentang itu, saya rasa saya akan sedikit menjelajah dan melihat apa yang terjadi!

I wonder about that, I think I'll discover bit by bit and see what happens!#1
Saya ragu mengenai itu, Saya berpikir akan menjelajah sedikit demi sedikit dan melihat apa yang akan terjadi!

I'd like to get a different style.
Saya tertarik untuk mendapatkan gaya yang berbeda.

I'll come visit! I hope that this island will give us what we're looking for...
Saya akan mengunjunginya! Saya berharap pulau ini dapat memberikan apa yang kita cari...

I'll get it!#0
Saya akan mendapatkannya!

I'll get it!#1
Saya akan mendapatkannya!

I'm Julia, my job is to register every body which enter or exit the ship.
Saya Julia, pekerjaan saya adalah untuk mendata siapapun yang akan masuk atau keluar kapal.

I'm busy#0
Saya sedang sibuk

I'm busy#1
Saya sedang sibuk

I've a girlfriend, and she needs me to do something unavailable.
Saya memiliki kekasih, dan dia menyuruh saya untuk melakukan sesuatu yang tidak mungkin dilakukan.

If you dont have it, then you cant dye.
Jika kamu tidak memilikinya, maka kamu tidak bisa mewarnai.

Imperial
Imperial

Infinitely long
Panjang tak terhingga

It's *hips* wrong, Imma not *hips* drunken.
Itu *tersendak* salah, Saya tidak *tersendak* mabuk.

Julia
Julia

Khaki
Khaki

Kota
Kota

Leave alone my family treasure!
Tinggalkan harta karun keluargaku!

Leave it
Tinggalkan

Let me see your work...
Biar saya lihat pekerjaanmu...

Let me sleep...
Biarkan saya tidur...

Let me think...
Biarkan saya berpikir...

Lettuce
Selada

Lime
Lemon

Lloyd
Lloyd

Long and curly
Panjang dan keriting

Long and slick
Panjang dan licin

Long ponytail
Kuncir panjang

Make less noise, please.
Tolong, jangan buat keributan.

Mane
Ekor kuda

Marko
Marko

Mauve
Ungu muda

Maybe you will be the next?#0
Mungkin kamu dapat menjadi yang selanjutnya ?

Maybe you will be the next?#1
Mungkin kamu akan menjadi yang selanjutnya?

Mini-skirt
Rok mini

Mohawk
Mohawk

Move along, kid.
Jalan terus, bocah.

Navy blue
Biru laut

No
Tidak

No problem, do you need something else?
Tidak masalah, apakah kamu membutuhkan sesuatu yang lain?

No!
Tidak!

No, thank you
Tidak, Terima kasih

No, thanks!
Tidak, terima kasih!

No, they are way too dangerous for me!
Tidak, mereka terlalu berbahaya untukku!

Not yet.
Belum.

Note
Catatan

Nothing, sorry
Maaf, Tidak ada.

Now i have class
Sekarang saya sedang ada kelas.

Of course! Tell me which language you speak.
Tentu saja! Katakan bahasa apa yang kamu gunakan.

Off black
Hitam pekat

Oh really? I don't have much time right now, I need to go. Sorry.
Oh benarkah? Saya tidak punya banyak waktu sekarang, Saya harus pergi. Maaf

Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!
Oh, Tidak apa-apa! Ngomong-Ngomong, jika kamu memiliki pertanyaan, kamu dapat menemui Julia di bagian resepsionis, dia sangat ramah!

Oh, thank you! Yo, what would I do without your help. Ok, see you!
Ok, terima kasih! Yo, saya tidak bisa melakukan apa apa tanpa bantuanmu. Ok, sampai jumpa!

Ok, Done. You need something else?
Ok, Selesai. Kamu membutuhkan yang lain?

Ok, ok. Come back if you change your mind.
Ok, ok. Datang kembali jika kamu berubah pikiran.

Ok, thanks
Ok, Terima kasih

Okay, I'm ready to work!#0
Oke, Saya siap untuk bekerja!

Okay, I'm ready to work!#1
Oke, Saya siap untuk bekerja!

Okay, you can start!
Oke, Kamu dapat memulainya!

Once it's done, you will be kicked from the server. Don't panic, as everything is fine.
Setelah kamu selesai, Kamu akan diusir dari server. Jangan panik, karena semua baik-baik saja.

Orange
Jingga

Pants
Celana dalam

Parted
Tersibak

Perky ponytail
Lebat kuncir ke belakang

Peter
Peter

Pigtails
Kuncir

Pink
Merah muda

Please change my race
Tolong ganti ras saya

Please do, my dear
Tolong lakukan, sayangku

Please select a race
Silahkan pilih ras

Pompadour
Berdiri

Punk
Punk

Purple
Ungu

Rabbit ears
Telinga kelinci

Read it
Baca itu

Red
Merah

Robe
 Jubah

Ronan
Ronan

Rrrr pchhhh...
Rrrr pchhhh...

Scarf
Selendang

Server
Server

ServiceNPC
ServisNPC

Shop 1
Toko 1

Shop 2
Toko 2

Shop 3
Toko 3

Shop 4
Toko 4

Shop 5
Toko 5

Shop 6
Toko 6

Shop 7
Toko 7

Short and curly
Pendek dan keriting

Short punk
Punk pendek

Short robe
Jubah pendek

Short tank top
Tank top pendek

Shorts
Celana pendek

Silver
Perak

Skirt
Rok

Sleeping Elf
Kurcaci tidur

So die already!
Jadi sudah mati!

Sorcerer robe
Jubah Penyihir

Sorry you dont have components.
Maaf anda tidak memiliki bahan-bahannya.

Surprise me
Kejutkan saya

T-neck sweater
Sweater kerah-T

Tank top
Tank top

Teal
Biru telur asin

TestNPC
TesNPC

There is also @@ on the docks, it will be a pleasure for her to help you!
Juga masih ada @@ di dermaga, dia akan dengan senang hati untuk menolongmu!

They are really nice and may help you much more than me.
Mereka Sangat baik dan mungkin akan lebih banyak membantumu daripada saya.

This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!
Sedikit lucu untuk memulai petualangan baru di pulau ini, dan juga ini adalah tempat yang bagus untuk bertemu orang-orang, haha!

This note was left by somebody.
Catatan ini tertinggal oleh seseorang

Tousled layers
Lapisan kusut

Turtles drop it. Good luck!
Kura-kura menjatuhkan itu. Semoga beruntung!

V-neck sweater
Sweater kerah-V

Warin
Warin

Warning: All characters under this login will be changed.
Peringatan: Semua karakter dengan login ini akan berubah.

Wave
Bergelombang

Wavy
Ikal Bergelombang

We are at [@@], it's a big port-city, many beginners start their adventure here.
Kita sedang berada di [@@], ini adalah kota pelabuhan yang besar, banyak pemula memulai petualangan mereka disini.

Wedding
Pernikahan

Well, I'll see what happens. I hope that good luck will follow me around!
Baik, Saya akan melihat apa yang akan terjadi. Saya harap keberuntungan akan selalu mengikutiku!

What can I dye for you today?
Apa yang dapat saya warnai untukmu hari ini ?

What color?
Warna apa?

What do you want to do with it?
Apa yang akan kamu lakukan dengan itu?

What do you want to dye?
Apa yang ingin kamu warnai ?

What do you want today?
Apa yang kamu inginkan hari ini?

What do you want?
Apa yang kamu inginkan?

What will you do during this break?
Apa yang akan kamu lakukan selama istirahat?

What? Never!
Apa? Tidak akan pernah!

What? This reward is too small!
Apa? Hadiah ini terlalu kecil!

Where are we?
Dimana kita?

Where is this cup?..*hips*
Dimana gelasnya?..*tersendak*

White
Putih

Why do you *hips* at me?!
Mengapa kamu *tersendak* ke saya?!

Why i cant dye my item?
Mengapa saya tidak bisa mewarnai barang-barang saya?

Why not, I need to train anyway.
Mengapa tidak, ngomong-ngomong saya butuh berlatih.

Wild
Liar

Witch hat
Topi penyihir

Would you be interested in a race change?
Apakah kamu tertarik untuk merubah ras ?

Would you be interested in a sex change?
Apakah kamu tertarik untuk mengubah jenis kelamin?

Yeah, but what reward will I get?
Ya, tetapi hadiah apa yang akan saya dapatkan?

Yeah, if you like, I'll open a market in this town, maybe you can visit me? It could be fun to see you again!
Ya, Jika kamu menyukainya, Saya akan membuka toko di kota ini, mungkin kamu dapat mengunjunginya ? Pasti menyenangkan jika Saya dapat menemuimu lagi!

Yellow
Kuning

Yes
Ya

Yes, I can!
Ya, saya bisa!

Yes, I did it!#0
Ya, Selesai!

Yes, I did it!#1
Ya, Selesai!

Yes, please!
Ya, dengan senang hati!

Yo, I found it, cool! I have everything, except @@ @@. Can you bring it to me?
Yo, Saya menemukannya, bagus! Saya memiliki segalanya, kecuali @@ @@. Dapatkah kamu membawakannya untukku?

Yo, can you help me, bro?
Yo, Dapatkah kamu membantuku, bro?

Yo, can you help me, sis?
Yo, dapatkah kamu membantuku, sis?

Yo, do you see what I mean?
Yo, apa kamu mengerti apa yang saya maksud ?

Yo, look: my friend advised me to make a cool and tasty meal, and he gave me instructions.
Yo, lihat: temanku menyuruhku untuk membuat makanan yang enak dan lezat, dan dia memberikanku petunjuknya.

Yo, now just bring it to me.
Yo, sekarang tinggal berikan itu padaku.

Yo, thank you!
Yo, terima kasih!

Yo, that's good that you want to help me.
Yo, itu bagus jika kamu mau menolongku.

Yo, you really brought it!
Yo, Kamu sungguh membawanya!

You can start now.
Kamu dapat memulainya sekarang.

You can't go there!
Kamu tidak dapat pergi kesana!

You don't have enough to pay for my services.
Kamu tidak punya cukup uang untuk membayar jasaku.

You failed the task!#0
Kamu mempersulit tugas ini!

You failed the task!#1
Kamu mengacaukan semuanya!

You should go see the sailors around here.#0
Kamu harus pergi menemui nelayan di sekitar sini.

You should go see the sailors around here.#1
Kamu harus pergi menemui nelayan di sekitar sini.

Zzzzzzzzzz
Zzzzzzzzzz

is helping me.
apakah membantu saya.

