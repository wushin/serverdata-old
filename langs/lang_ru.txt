Copyright (C) 2010-2011  Evol Online
*hips*
*ик*

... By the power vested in me,
... Силой возложенной на меня,

Ah, she *hips* is here.
Ах, она *ик* здесь.

Are you tired of being what you are?
Ты устал быть тем, кем ты являешься?

AreaNPC
AreaNPC

As you want!
Как пожелаешь!

Ash brown
Коричневый

Bald
Лысый

Bee spawned
Пчела создана

Billy Bons
Билли Бонс

Black
Черный

Blaf
Блаф

Ble *hips*... Imma great sail*hips*or!
Бле *ик* ... Яаа великий мор*ик*як!

Blonde
Блондин

Blue
Голубой

Boots
Ботинки

Bowl cut
Горшок

Brown
Коричневый

Brunette
Брюнет

Bun
Булка

Bunny ears
Кроличьи уши

Bye bye!
Пока-пока!

Can you do something with my color?
Вы можете изменить мой цвет?

Cap
Кепка

Center parting/Short and slick
Разделенные / короткие и гладкие

Choppy
Порывистый

Combed back
Собранные сзади

Continue?
Продолжить?

Copper
Чопер

Copper red
Нежно-красный

Could I ask you which language do you talk? Like that I add you on the list.
Могу я узнать на каком языке ты разговариваешь? Я смогу записать тебя в список.

Demon
Демон

Desert hat
Пустынная шапка

Devis
Девис

Did you bring me @@ @@?#0
Ты принесла мне @@ @@?

Did you bring me @@ @@?#1
Ты принес мне @@ @@?

Do you know some interesting people around here?
Ты знаешь каких-нибудь интересных людей в округе?

Do you want to create a bee?
Ты хочешь создать пчелу?

Don't worry, I'm sure that everything will turn out well for the both of us!
Не волнуйся, я думаю у нас все будет нормально!

Don't you see that I'm already talking?
Ты не видишь что я уже разговариваю?

Done.
Сделано.

Dyeing failed. You lost dye item.
Покраска не удалась. Вы потеряли краску.

Dyer
Красильщик

Elmo
Элмо

Emo
Эмо

Enora
Энора

Eurni
Еурни

Eurni the Surgeon
Хирург Еурни

Flat ponytail
Плоский хвост

For dye your item, you need white item and dye with required color.
Чтобы покрасить что-то, тебе необходима вещь белого цвета и краска необходимого цвета.

Fushia
Фушия

Ggrmm grmmm..
Ггрмм грммм..

Gloves
Перчатки

Good job!
Хорошая работа!

Green
Зеленый

Hahaha, I won't take much of your time, go ahead!
Ха-ха, не хочу занимать у тебя много времени, продолжай!

He takes a small list and starts reading it.
Он достал небольшой список и начал читать.

Headband
Повязка для головы

Hehe. Me, I will try to open a market, I've heard that it is a big city of commerce here, because of the port and all...
Хе-хе, я попробую открыть рынок, я слышал, что здесь большой торговый город, из-за порта и всего остального...

Hello @@
Привет @@

Hello dear, what do you want today?#0
Привет дорогая, что ты хочешь сегодня?

Hello dear, what do you want today?#1
Привет дорогой, что ты хочешь сегодня?

Hello, girl!
Здравствуй, девушка!

Hello, stranger!
Здравствуй, незнакомец!

Here is your reward!
Вот твоя награда!

Hey! Yes, I'm already awake. So, it's end of our little road together, what will you do you now?
Привет! Да, я уже проснулся. Итак, это конец нашего совместного пути, что ты теперь будешь делать?

Hey! You saw? we've arrived on the port today, it was a long way from the other island...#0
Привет! Ты видела? Мы прибыли в портовый город, это был долгий путь с другого острова...

Hey! You saw? we've arrived to the port today, it was a long way from the other island...#1
Привет! Ты видел? Мы прибыли в портовый город, это был долгий путь с другого острова...

Hey, girl!
Привет, девушка!

Hey, guy!
Привет, парень!

Hey, look at Julia, she's a beauty, isn't she? She's been looking at me for some minutes now... haha! Good luck, my friend.
Эй, посмотри на Джулию, она красива, да? Она смотрит на меня уже несколько минут... ха-ха! удачи, мой друг.

Hey, you lied to me.
Эй, ты врешь мне.

Hmm, I'm fine for now, thank you.
Хмм, я и так доволен. спасибо.

How can I get it
Как я могу это достать

I can!
Я могу!

I don't need your help right now, come back later.
Сейчас мне не нужна твоя помощь. Приходи позже.

I don't want change my language, sorry.
Я не хочу менять свой язык, прости.

I gave you a reward. I can't give you more.
Я дал тебе награду. Я не могу дать тебе больше.

I made a mistake, I would like to change my language.#0
Я сделала ошибку и хотела бы поменять свой язык.

I made a mistake, I would like to change my language.#1
Я сделал ошибку и хотел бы поменять свой язык.

I need help for cleaning the wedge of the ship, but you aren't strong enough to help me.
Мне нужна помощь, чтобы очистить корабль, но ты слишком слаб для этого.

I need somebody who can clean the bottom of the ship, can you help me?
Мне нужен кто-то, кто может очистить нижнюю часть корабля. Можешь мне помочь?

I sentence you to death... twice.
Я приговариваю тебя к смерти... дважды.

I speak Dutch
Я говорю на голландском

I speak English
Я говорю на английском

I speak Flemish
Я говорю на фламандском

I speak French
Я говорю на французском

I speak Greek
Я говорю на греческом

I speak Indonesian


I speak Italian
Я говорю на итальянском

I speak Russian
Я говорю на русском

I speak Spanish
Я говорю на испанском

I think that I will explore around a bit and see what I can find, and you?
Я думаю, что исследую окрестности, и посмотрю, что смогу сделать, а ты?

I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.
Я думаю, что корабль будет здесь некоторое время. Нам нужно много еды, и наш корабль нуждается в небольшом ремонте перед тем как отправляться обратно в море.

I think you can help me, right?
Я думаю ты мне можешь помочь, правильно?

I want to be a Human
Я хочу быть человеком

I want to be a Ifitron
Я хочу быть ифитроном

I want to be a Orc
Я хочу быть орком

I want to be a Sparron
Я хочу быть спарроном

I want to be a Ukar
Я хочу быть укаром

I want to dye to different color
Я хочу покрасить в другой цвет

I will give you @@gp.
Я дам тебе @@gp.

I will need some coffee...
Мне нужно немного кофе...

I will rest a bit here or in the town, I don't feel so well after this trip...
Я отдохну немного здесь или в городе, я чувствую себя не слишком хорошо после этого путешествия...

I will!
Я выполню!

I wonder about that, I think I'll discover bit by bit and see what happens!#0
Я удивлена. Я думаю, я буду исследовать тут все понемногу, а потом посмотрим, что из этого выйдет!

I wonder about that, I think I'll discover bit by bit and see what happens!#1
Я удивлен. Я думаю, я буду исследовать тут все понемногу, а потом посмотрим, что из этого выйдет!

I'd like to get a different style.
Я бы хотел сменить стиль.

I'll come visit! I hope that this island will give us what we're looking for...
Я еще зайду! Я надеюсь что этот остров даст нам то, что мы ищем...

I'll get it!#0
Я сделаю это!

I'll get it!#1
Я сделаю это!

I'm Julia, my job is to register every body which enter or exit the ship.
Я Джулия, моя работа - записывать всех, кто заходит на корабль или выходит с него.

I'm busy#0
Я занята

I'm busy#1
Я занят

I've a girlfriend, and she needs me to do something unavailable.
У меня есть подружка, и она хочет, чтобы я достал то, чего ей не хватает.

If you dont have it, then you cant dye.
Если у тебя этого нет, ты не можешь красить.

Imperial
Имперский

Infinitely long
Бесконечную длину

It's *hips* wrong, Imma not *hips* drunken.
Это *ик* неправда, яя не *ик* пьяный.

Julia
Джулия

Khaki
Хаки

Kota
Кота

Leave alone my family treasure!
Оставьте в покое мое сокровище!

Leave it
Оставить его

Let me see your work...
Дай мне посмотреть на твою работу...

Let me sleep...
Дай мне поспать...

Let me think...
Дай мне подумать...

Lettuce
Салат

Lime
Лимон

Lloyd
Ллойд

Long and curly
Длинные и волнистые

Long and slick
Длинные и гладкие

Long ponytail
Длинный хвост

Make less noise, please.
Не шуми, пожалуйста.

Mane
Грива

Marko
Марко

Mauve
Розовый

Maybe you will be the next?#0
Может быть ты будешь следующей?

Maybe you will be the next?#1
Может быть ты будешь следующим?

Mini-skirt
Мини-юбка

Mohawk
Мохоук

Move along, kid.
Уходи отсюда, ребенок.

Navy blue
Тёмно-синий

No
Нет

No problem, do you need something else?
Нет проблем, тебе нужно что-то еще?

No!
Нет!

No, thank you
Нет, спасибо

No, thanks!
Не, спасибо!

No, they are way too dangerous for me!
Нет, они слишком опасны для меня!

Not yet.
Еще нет.

Note
Записка

Nothing, sorry
Ничего, извините

Now i have class
Сейчас у меня класс

Of course! Tell me which language you speak.
Конечно! Скажи мне, на каком языке ты разговариваешь.

Off black
Черный

Oh really? I don't have much time right now, I need to go. Sorry.
В самом деле? У меня нет времени. Мне надо идти. Извини.

Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!
О, без проблем! В любом случае, если тебе надо что-то узнать, иди к Джулии, она очень любезна!

Oh, thank you! Yo, what would I do without your help. Ok, see you!
О, спасибо! Йо, чтобы я делал без твоей помощи. Увидимся!

Ok, Done. You need something else?
Сделано. Тебе еще что-то нужно?

Ok, ok. Come back if you change your mind.
Хорошо, хорошо. Возвращайся если передумаешь.

Ok, thanks
Хорошо, спасибо

Okay, I'm ready to work!#0
Хорошо, я готова к работе!

Okay, I'm ready to work!#1
Хорошо, я готов к работе!

Okay, you can start!
Хорошо, можешь начинать!

Once it's done, you will be kicked from the server. Don't panic, as everything is fine.
Когда это закончится, ты будешь выкинут с сервера. Не паникуй, т.к. все впорядке.

Orange
Оранжевый

Pants
Брюки

Parted
Разделенный

Perky ponytail
Бродящий хвост

Peter
Петр

Pigtails
Косички

Pink
Розовый

Please change my race
Пожалуйста, измени мою расу

Please do, my dear
Сделай это, дорогой

Please select a race
Пожалуйста, выбери расу

Pompadour
Помпадур

Punk
Панк

Purple
Фиолетовый

Rabbit ears
Заячьи уши

Read it
Прочитать

Red
Красный

Robe
Мантия

Ronan
Ронан

Rrrr pchhhh...
Рррр пчхххх...

Scarf
Шарф

Server
Сервер

ServiceNPC
СервисныйНИП

Shop 1
Магазин 1

Shop 2
Магазин 2

Shop 3
Магазин 3

Shop 4
Магазин 4

Shop 5
Магазин 5

Shop 6
Магазин 6

Shop 7
Магазин 7

Short and curly
Короткий и фигурный

Short punk
Короткий панк

Short robe
Котрокая мантия

Short tank top
Короткая броня

Shorts
Шорты

Silver
Серебро

Skirt
Юбка

Sleeping Elf
Спящий эльф

So die already!
Так умри же!

Sorcerer robe
Мантия волшебника

Sorry you dont have components.
Извини, у тебя не хватает компонентов.

Surprise me
Удиви меня

T-neck sweater
Т-образный свитер

Tank top
Броня

Teal
Чирок

TestNPC
ТестовыйНИП

There is also @@ on the docks, it will be a pleasure for her to help you!
Еще ты можешь найти @@ в доках. Для нее будет удовольствием помочь тебе!

They are really nice and may help you much more than me.
Они хорошие и могут помочь тебе больше, чем я.

This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!
Этот остров забавен для начала приключений, и это хорошее место для встречи с другими людьми. хаха!

This note was left by somebody.
Эта записка была кем-то оставлена.

Tousled layers
Растрепанные слои

Turtles drop it. Good luck!
Черепахи оставляют его. Удачи!

V-neck sweater
V-образный свитер

Warin
Варин

Warning: All characters under this login will be changed.
Предупреждение: Все персонажи, принадлежащие текущему логину, будут изменены.

Wave
Волна

Wavy
Волнистый

We are at [@@], it's a big port-city, many beginners start their adventure here.
Мы в [@@], это большой портовый город. Многие начинают здесь свои приключения.

Wedding
Свадебный

Well, I'll see what happens. I hope that good luck will follow me around!
Ну, я посмотрю что будет. Я надеюсь, что удача будет на моей стороне!

What can I dye for you today?
Что я могу покрасить тебе сегодня?

What color?
Какой цвет?

What do you want to do with it?
Что ты хочешь с ним сделать?

What do you want to dye?
Что ты хочешь покрасить?

What do you want today?
Что тебе сегодня нужно?

What do you want?
Что ты хочешь?

What will you do during this break?
Что ты будешь делать во время этой остановки?

What? Never!
Что? Никогда!

What? This reward is too small!
Что? Эта награда слишком мала!

Where are we?
Где мы?

Where is this cup?..*hips*
Где этот стакан?..*ик**

White
Белый

Why do you *hips* at me?!
Почему ты *ик* на меня?!

Why i cant dye my item?
Почему я не могу покрасить свои вещи?

Why not, I need to train anyway.
Почему нет, мне все равно надо тренироваться.

Wild
Дикий

Witch hat
Шляпа ведьмы

Would you be interested in a race change?
Возможно ты хочешь поменять свою расу?

Would you be interested in a sex change?
Может быть ты хочешь поменять свой пол?

Yeah, but what reward will I get?
Да, но какую награду я получу?

Yeah, if you like, I'll open a market in this town, maybe you can visit me? It could be fun to see you again!
Да, если тебе интересно, я открою рынок в этом городе, может быть ты сможешь навестить меня? Буду рад увидеть тебя снова!

Yellow
Желтый

Yes
Да

Yes, I can!
Да, я могу!

Yes, I did it!#0
Да, я сделала это!

Yes, I did it!#1
Да, я сделал это!

Yes, please!
Да, пожалуйста!

Yo, I found it, cool! I have everything, except @@ @@. Can you bring it to me?
Йо, я думаю это круто! У меня есть все, за исключением @@ @@. Ты можешь принести их мне?

Yo, can you help me, bro?
Йо, ты можешь мне помочь брат?

Yo, can you help me, sis?
Йо, ты можешь мне помочь сестра?

Yo, do you see what I mean?
Йо, ты понимаешь, что я имею в виду?

Yo, look: my friend advised me to make a cool and tasty meal, and he gave me instructions.
Йо, смотри, моя подружка поручила мне приготовить вкусную еду и дала мне рецепт.

Yo, now just bring it to me.
Йо, просто принеси мне его.

Yo, thank you!
Йо, спасибо!

Yo, that's good that you want to help me.
Йо, это хорошо, что ты хочешь помочь мне.

Yo, you really brought it!
Йо, ты действительно принес это!

You can start now.
Ты можешь начинать.

You can't go there!
Ты не можешь пройти сюда!

You don't have enough to pay for my services.
У тебя недостаточно денег, чтобы оплатить мои услуги.

You failed the task!#0
Ты провалила задание!

You failed the task!#1
Ты провалил задание!

You should go see the sailors around here.#0
Ты должна встретить моряков поблизости.

You should go see the sailors around here.#1
Ты должен встретить моряков поблизости.

Zzzzzzzzzz
Zzzzzzzzzz

is helping me.
помогает мне.

