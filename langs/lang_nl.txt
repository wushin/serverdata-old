Copyright (C) 2010-2011  Evol Online
*hips*
*hips*

... By the power vested in me,
... Door de macht toegekend aan mij,

Ah, she *hips* is here.
Ah, ze *hips* is hier.

Are you tired of being what you are?
Ben jij het moe om uzelf te zijn?

AreaNPC
AreaNPC

As you want!
Zoals je wil!

Ash brown
Asbruin

Bald
Kaal

Bee spawned
Bij gespawned

Billy Bons
Billy Bons

Black
Zwart

Blaf
Woef

Ble *hips*... Imma great sail*hips*or!
Bwe *hips*... Ikke ben ne goeie zeil*hips*er!

Blonde
Blond

Blue
Blauw

Boots
Laarzen

Bowl cut
Gesneden gelijk een pot

Brown
Bruin

Brunette
Bruin

Bun
Bun

Bunny ears
Konijn oortjes

Bye bye!
Daaag!

Can you do something with my color?
Kan je iets aan mijn kleur doen?

Cap
Pet

Center parting/Short and slick
Center parting/Short and slick

Choppy
Choppy

Combed back
Naar achter gekamd

Continue?
Verder gaan?

Copper
Koper

Copper red
Roest

Could I ask you which language do you talk? Like that I add you on the list.
Mag ik je vragen welke taal jij spreekt? Zodat ik jou kan toevoegen aan de lijst.

Demon
Demon

Desert hat
Woestijnhoed

Devis
Devis

Did you bring me @@ @@?#0
Heb je mij @@ @@ gebracht?

Did you bring me @@ @@?#1
Heb je mij @@ @@ gebracht?

Do you know some interesting people around here?
Ken je enkele interessante personen in de buurt?

Do you want to create a bee?
Wil je een bij spawnen?

Don't worry, I'm sure that everything will turn out well for the both of us!
Maak je geen zorgen, ik ben zeker dat alles goed zal uitdraaien voor ons beiden!

Don't you see that I'm already talking?
Zie je niet dat ik al aan het spreken ben?

Done.
Klaar.

Dyeing failed. You lost dye item.
Het verkleuren faalde. U bent het verkleuringsitem kwijt.

Dyer
Kleurder

Elmo
Elmo

Emo
Emo

Enora
Enora

Eurni
Eurni

Eurni the Surgeon
Eurni de Chirurg

Flat ponytail
Platte ponytail

For dye your item, you need white item and dye with required color.
Om iets te verkleuren, moet je een wit item hebben, een kleuroplossing in de gewenste kleur.

Fushia
Fuchia

Ggrmm grmmm..
Ggrmm grmmm..

Gloves
Handschoenen

Good job!
Goed gedaan!

Green
Groen

Hahaha, I won't take much of your time, go ahead!
Hahaha, Ik zal niet veel van je tijd innemen, ga je gang!

He takes a small list and starts reading it.
He neemt een kleine lijst en begint het te lezen.

Headband
Hoofdband

Hehe. Me, I will try to open a market, I've heard that it is a big city of commerce here, because of the port and all...
Hehe. Ik, ik zal proberen een markt te openen, ik heb gehoord dat het hier een grote commerciële stad is, door de haven enzo...

Hello @@
Hallo @@

Hello dear, what do you want today?#0
Hallo, wat wil je vandaag?

Hello dear, what do you want today?#1
Hallo, wat wil je vandaag?

Hello, girl!
Hallo, meisje!

Hello, stranger!
Hallo, vreemdeling!

Here is your reward!
Hier is je beloning!

Hey! Yes, I'm already awake. So, it's end of our little road together, what will you do you now?
Hey! Ja, ik ben al wakker. Dus, dit is het einde van onze kleine reis tesamen, wat wil je nu doen?

Hey! You saw? we've arrived on the port today, it was a long way from the other island...#0
Hey! Heb je 't gezien? we zijn aangekomen in de haven vandaag, het was een lange weg van het andere eiland...

Hey! You saw? we've arrived to the port today, it was a long way from the other island...#1
Hey! Heb je 't gezien? we zijn aangekomen in de haven vandaag, het was een lange weg van het andere eiland...

Hey, girl!
Hey, meisje!

Hey, guy!
Hey, jongen!

Hey, look at Julia, she's a beauty, isn't she? She's been looking at me for some minutes now... haha! Good luck, my friend.
Hey, kijk naar Julia, is ze niet een plaatje? Ze is al sinds enkele minuten naar mij aan het staren... haha! Veel geluk, mijn vriend.

Hey, you lied to me.
Hey, je hebt tegen mij gelogen.

Hmm, I'm fine for now, thank you.
Hmm, Ik ben ok nu, dank je.

How can I get it
Hoe kan ik het krijgen

I can!
I kan het!

I don't need your help right now, come back later.
Ik heb je help nu niet nodig, kom later maar terug.

I don't want change my language, sorry.
Ik wil mijn taal nu niet wijzigen, sorry.

I gave you a reward. I can't give you more.
Ik heb je een beloning gegeven. Ik kan je niks meer geven.

I made a mistake, I would like to change my language.#0
Ik heb een fout gemaakt, ik zou graag mijn taal wijzigen.

I made a mistake, I would like to change my language.#1
Ik heb een fout gemaakt, ik zou graag mijn taal wijzigen.

I need help for cleaning the wedge of the ship, but you aren't strong enough to help me.
Ik heb hulp nodig om de wig van het schip schoon te maken, maar jij bent niet sterk genoeg om me te helpen.

I need somebody who can clean the bottom of the ship, can you help me?
Ik heb iemand nodig die de onderkant van het schip kan schoonmaken, kan je me helpen?

I sentence you to death... twice.
Ik verklaar je tot de dood... twee maal.

I speak Dutch
Ik spreek Nederlands

I speak English
Ik spreek Engels

I speak Flemish
Ik spreek West-Vlaams

I speak French
Ik spreek Frans

I speak Greek


I speak Indonesian


I speak Italian
Ik spreek Italiaans

I speak Russian
Ik spreek Russisch

I speak Spanish
Ik spreek Spaans

I think that I will explore around a bit and see what I can find, and you?
Ik denk dat ik een beetje zal rondkijken en zien dat ik vind, jij?

I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.
Ik denk dat het schip hier nog wel even zal staan, we hebben een hoop eten nodig en ons schip heeft ook wat reparatie nodig voordat het terug zeevaardig is.

I think you can help me, right?
Ik denk dat je mij kan helpen, juist?

I want to be a Human
Ik wil een Mens worden

I want to be a Ifitron
Ik zou graag een Ifitron worden

I want to be a Orc
Ik wil een Orc worden

I want to be a Sparron
Ik zou graag een Sparron worden

I want to be a Ukar
Ik wil een Ukar worden

I want to dye to different color
Ik zou graag in een andere kleur verkleuren.

I will give you @@gp.
Ik zal je @@gp geven.

I will need some coffee...
Ik wil wat koffie hebben...

I will rest a bit here or in the town, I don't feel so well after this trip...
Ik zal hier een beetje rusten in het dorp, ik voel met niet zo goed na deze reis...

I will!
Ik zal het doen!

I wonder about that, I think I'll discover bit by bit and see what happens!#0
Ik vraag 't me af, ik denk dat ik beetje bij beetje zal ontdekken en zien wat er gebeurt!

I wonder about that, I think I'll discover bit by bit and see what happens!#1
Ik vraag 't me af, ik denk dat ik beetje bij beetje zal ontdekken en zien wat er gebeurt!

I'd like to get a different style.
Ik zou graag een andere stijl willen.

I'll come visit! I hope that this island will give us what we're looking for...
Ik zal je komen bezoeken! Ik hoop dat dit eiland onz zal geven waar we op zoek naar zijn...

I'll get it!#0
Ik zal het even halen!

I'll get it!#1
Ik zal het even halen!

I'm Julia, my job is to register every body which enter or exit the ship.
Ik ben Julia, mijn job is om iedereen te registreren die van het schip komt en gaat.

I'm busy#0
Ik ben bezig

I'm busy#1
Ik ben bezig

I've a girlfriend, and she needs me to do something unavailable.
Ik een vriendin, en ze vraagt mij iets onmogelijks.

If you dont have it, then you cant dye.
Als u het niet heeft, dan kan u niet iets verkleuren...

Imperial
Imperiaal

Infinitely long
Oneindig lang

It's *hips* wrong, Imma not *hips* drunken.
Het is *hips* verkeerd, Ikke ben niet *hips* dronken.

Julia
Julia

Khaki
Kaki

Kota
Kota

Leave alone my family treasure!
Laat mijn familie juwelen met rust!

Leave it
Laat het liggen

Let me see your work...
Laat me je werk eens zien...

Let me sleep...
Laat me slapen...

Let me think...
Laat me even denken...

Lettuce
Sla

Lime
Lichtgroen

Lloyd
Lloyd

Long and curly
Lang en krullend

Long and slick
Lang en recht

Long ponytail
Lang ponytail

Make less noise, please.
Maak wat minder lawaai, aub.

Mane
Manen

Marko
Marko

Mauve
Mauve

Maybe you will be the next?#0
Misschien word jij de volgende?

Maybe you will be the next?#1
Misschien word jij de volgende?

Mini-skirt
Minirok

Mohawk
Mohawk

Move along, kid.
Ga maar verder, kind.

Navy blue
Marineblauw

No
Nee

No problem, do you need something else?
Geen probleem, wil je nog iets anders?

No!
Nee!

No, thank you
Nee, dank je

No, thanks!
Nee, danku!

No, they are way too dangerous for me!
Nee, Ze zijn veel te gevaarlijk voor me!

Not yet.
Nog niet..

Note
Nota

Nothing, sorry
Niks, sorry

Now i have class
Nu heb ik klasse

Of course! Tell me which language you speak.
Natuurlijk! Vertel me welke taal je spreekt.

Off black
Bijna zwart

Oh really? I don't have much time right now, I need to go. Sorry.
Oh echt? Ik heb niet veel tijd nu, ik moet gaan. Sorry.

Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!
Oh, geen probleem! In elk geval, als je vragen hebt, kan je 't vragen aan Julia aan de receptie, ze is heel vriendelijk!

Oh, thank you! Yo, what would I do without your help. Ok, see you!
Oh, dank je! Yo, wat zou ik zonder je help doen. Ok, ik zie je nog wel!

Ok, Done. You need something else?
Ok, Klaar. Heb je nog iets anders nodig?

Ok, ok. Come back if you change your mind.
Ok, ok. Kom maar terug als je van gedacht veranderd.

Ok, thanks
Ok, dank u

Okay, I'm ready to work!#0
Oke, Ik ben klaar om te werken!

Okay, I'm ready to work!#1
Oke, Ik ben klaar om te werken!

Okay, you can start!
Oke, je kan starten!

Once it's done, you will be kicked from the server. Don't panic, as everything is fine.
Als 't klaar is, zal je gekicked worden van de server. Niet panikeren, dat is volkomen normaal.

Orange
Oranje

Pants
Broek

Parted
Gesplitst

Perky ponytail
Parmantige ponytail

Peter
Peter

Pigtails
Vlechtjes

Pink
Roze

Please change my race
Verander mijn ras

Please do, my dear
Doe maar

Please select a race
Kies een ras

Pompadour
Pompadour

Punk
Punk

Purple
Roze

Rabbit ears
Konijn oortjes

Read it
Lees het

Red
Rood

Robe
Kleed

Ronan
Ronan

Rrrr pchhhh...
Rrrr pchhhh...

Scarf
Sjaal

Server
Server

ServiceNPC
ServiceNPC

Shop 1
Shop 1

Shop 2
Shop 2

Shop 3
Shop 3

Shop 4
Shop 4

Shop 5
Shop 5

Shop 6
Shop 6

Shop 7
Shop 7

Short and curly
Kort en krullend

Short punk
Korte punk

Short robe
Kort kleed

Short tank top
Korte spaghettibandjes

Shorts
Short

Silver
Zilver

Skirt
Rok

Sleeping Elf
Slapende Elf

So die already!
Dus ga alsjeblieft zo snel mogelijk dood!

Sorcerer robe
Tovenaarskleed

Sorry you dont have components.
Sorry, je hebt geen componenten.

Surprise me
Verras me

T-neck sweater
Coltrui

Tank top
Spaghettibandjes

Teal
Groenblauw

TestNPC
TestNPC

There is also @@ on the docks, it will be a pleasure for her to help you!
Er is ook @@ aan de dokken, het zal een plezier zijn voor haar om je te helpen!

They are really nice and may help you much more than me.
Ze zijn echt vriendelijk en kunnen je misschien veel meer verder helpen dan mij.

This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!
Dit eiland is nogal grappig om een avontuur te starten, en het is ook een goeie plaats om mensen te ontmoeten, haha!

This note was left by somebody.
Deze nota was achtergelaten door iemand.

Tousled layers
Warrige lagen

Turtles drop it. Good luck!
Schilpadden laten het vallen. Veel geluk!

V-neck sweater
V-hals trui

Warin
Warin

Warning: All characters under this login will be changed.
Waarschuwing: Alle karakters bij deze login zullen worden veranderd.

Wave
Zwaai

Wavy
Golvend

We are at [@@], it's a big port-city, many beginners start their adventure here.
We zijn in [@@], dat is een grote havenstad, veel beginners starten hun avondtuur hier.

Wedding
Trouw

Well, I'll see what happens. I hope that good luck will follow me around!
Wel, Ik zal wel zien wat er gebeurt. Ik hoop dat ik veel geluk zal hebben!

What can I dye for you today?
Wat kan ik voor je kleuren vandaag?

What color?
Welke kleur?

What do you want to do with it?
Wat wil je ermee doen?

What do you want to dye?
Wat wil je verkleuren?

What do you want today?
Wat wil je vandaag doen?

What do you want?
Wat wil je?

What will you do during this break?
Wat zal je doen gedurende deze pauze?

What? Never!
Wat? Nooit!

What? This reward is too small!
Wat? Deze beloning is veel te klein!

Where are we?
Waar zijn we?

Where is this cup?..*hips*
Waar is deze beker?..*hips*

White
Wit

Why do you *hips* at me?!
Waarom doe je *hips* naar mij?!

Why i cant dye my item?
Waarom kan ik mijn item niet verkleuren?

Why not, I need to train anyway.
Waarom niet, Ik moet toch nog wat trainen.

Wild
Wild

Witch hat
Heksenhoed

Would you be interested in a race change?
Zou u geinteresseerd zijn in een ras verandering?

Would you be interested in a sex change?
Zou u geinteresseerd zijn in een geslachts verandering?

Yeah, but what reward will I get?
Mja, maar welke beloning krijg ik?

Yeah, if you like, I'll open a market in this town, maybe you can visit me? It could be fun to see you again!
Mja, als je wil, Zal ik een markt openen in dit dorp, misschien kan je me komen bezoeken? Het zou heel leuk zijn om je opnieuw te zien!

Yellow
Geel

Yes
Ja

Yes, I can!
Ja, dat kan ik!

Yes, I did it!#0
Ja, ik heb het gedaan!

Yes, I did it!#1
Ja, ik heb het gedaan!

Yes, please!
Ja, aub!

Yo, I found it, cool! I have everything, except @@ @@. Can you bring it to me?
Yo, Ik heb het gevonden, cool! Ik heb alles, behalve @@ @@. Kan je me dat brengen?

Yo, can you help me, bro?
Yo, kan je me helpen, man?

Yo, can you help me, sis?
Yo, kan je me helpen, vrouw?

Yo, do you see what I mean?
Yo, zie je wat ik bedoel?

Yo, look: my friend advised me to make a cool and tasty meal, and he gave me instructions.
Yo, zie: mijn vriend heeft me geadviseerd om een coole en lekkere maaltijd te maken, en hij heeft me instructies gegeven.

Yo, now just bring it to me.
Yo, geef het me nu maar.

Yo, thank you!
Yo, dank je!

Yo, that's good that you want to help me.
Yo, het is goed dat je me wilt helpen.

Yo, you really brought it!
Yo, je hebt het echt gebracht!

You can start now.
Je kan nu starten.

You can't go there!
Je mag daar niet in gaan!

You don't have enough to pay for my services.
Je hebt niet genoeg geld om voor mijn diensten te betalen.

You failed the task!#0
Je hebt je taak gefaald!

You failed the task!#1
Je hebt je taak gefaald!

You should go see the sailors around here.#0
Je zou de zeilers in de buurt kunnen aanspreken.

You should go see the sailors around here.#1
Je zou de zeilers in de buurt kunnen aanspreken.

Zzzzzzzzzz
Zzzzzzzzzz

is helping me.
is me aan het helpen.

