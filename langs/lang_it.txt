Copyright (C) 2010-2011  Evol Online
*hips*


... By the power vested in me,


Ah, she *hips* is here.


Are you tired of being what you are?
Sei stanco/a di essere ciò che sei?

AreaNPC


As you want!
Come desideri!

Ash brown


Bald
Pelato

Bee spawned
Ape creata

Billy Bons


Black
Neri

Blaf
Blaf

Ble *hips*... Imma great sail*hips*or!


Blonde
Biondi

Blue
Blu

Boots


Bowl cut
Taglio a scodella

Brown


Brunette
Bruni

Bun
Chignon

Bunny ears


Bye bye!
Ciao ciao!

Can you do something with my color?


Cap


Center parting/Short and slick
Divisi al centro/Corti e lisci

Choppy
Increspati

Combed back
Pettinati all'indietro

Continue?
Continuare?

Copper
Rame

Copper red


Could I ask you which language do you talk? Like that I add you on the list.


Demon


Desert hat


Devis


Did you bring me @@ @@?#0


Did you bring me @@ @@?#1


Do you know some interesting people around here?


Do you want to create a bee?
Vuoi creare un'ape?

Don't worry, I'm sure that everything will turn out well for the both of us!


Don't you see that I'm already talking?


Done.
Finito.

Dyeing failed. You lost dye item.


Dyer


Elmo


Emo
Emo

Enora


Eurni
Eruni

Eurni the Surgeon
Eruni il Chirurgo

Flat ponytail
Coda di cavallo piatta

For dye your item, you need white item and dye with required color.


Fushia


Ggrmm grmmm..


Gloves


Good job!


Green
Verde

Hahaha, I won't take much of your time, go ahead!


He takes a small list and starts reading it.


Headband


Hehe. Me, I will try to open a market, I've heard that it is a big city of commerce here, because of the port and all...


Hello @@


Hello dear, what do you want today?#0
Ciao cara, cosa vuoi oggi?

Hello dear, what do you want today?#1
Ciao caro, cosa vuoi oggi?

Hello, girl!


Hello, stranger!


Here is your reward!


Hey! Yes, I'm already awake. So, it's end of our little road together, what will you do you now?


Hey! You saw? we've arrived on the port today, it was a long way from the other island...#0


Hey! You saw? we've arrived to the port today, it was a long way from the other island...#1


Hey, girl!


Hey, guy!


Hey, look at Julia, she's a beauty, isn't she? She's been looking at me for some minutes now... haha! Good luck, my friend.


Hey, you lied to me.


Hmm, I'm fine for now, thank you.


How can I get it


I can!


I don't need your help right now, come back later.


I don't want change my language, sorry.


I gave you a reward. I can't give you more.


I made a mistake, I would like to change my language.#0


I made a mistake, I would like to change my language.#1


I need help for cleaning the wedge of the ship, but you aren't strong enough to help me.


I need somebody who can clean the bottom of the ship, can you help me?


I sentence you to death... twice.


I speak Dutch


I speak English


I speak Flemish


I speak French


I speak Greek


I speak Indonesian


I speak Italian


I speak Russian


I speak Spanish


I think that I will explore around a bit and see what I can find, and you?


I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.


I think you can help me, right?


I want to be a Human
Voglio essere un Umano

I want to be a Ifitron


I want to be a Orc


I want to be a Sparron


I want to be a Ukar
Voglio essere un Ukar

I want to dye to different color


I will give you @@gp.


I will need some coffee...


I will rest a bit here or in the town, I don't feel so well after this trip...


I will!


I wonder about that, I think I'll discover bit by bit and see what happens!#0


I wonder about that, I think I'll discover bit by bit and see what happens!#1


I'd like to get a different style.


I'll come visit! I hope that this island will give us what we're looking for...


I'll get it!#0


I'll get it!#1


I'm Julia, my job is to register every body which enter or exit the ship.


I'm busy#0


I'm busy#1


I've a girlfriend, and she needs me to do something unavailable.


If you dont have it, then you cant dye.


Imperial
Imperiale

Infinitely long
Infinitamente lungo

It's *hips* wrong, Imma not *hips* drunken.


Julia


Khaki


Kota


Leave alone my family treasure!
Lascia stare i miei gioelli di famiglia!

Leave it


Let me see your work...


Let me sleep...


Let me think...


Lettuce


Lime


Lloyd


Long and curly
Lunghi e ricci

Long and slick
Lunghi e lisci

Long ponytail
Coda di cavallo lunga

Make less noise, please.


Mane
Criniera

Marko
Marko

Mauve


Maybe you will be the next?#0


Maybe you will be the next?#1


Mini-skirt


Mohawk
Moicano

Move along, kid.
Vattene bimbo.

Navy blue


No
No

No problem, do you need something else?


No!


No, thank you
No, ti ringrazio

No, thanks!


No, they are way too dangerous for me!


Not yet.


Note


Nothing, sorry
Nulla, scusa per il disturbo

Now i have class
Ora ho la classe

Of course! Tell me which language you speak.


Off black


Oh really? I don't have much time right now, I need to go. Sorry.


Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!


Oh, thank you! Yo, what would I do without your help. Ok, see you!


Ok, Done. You need something else?


Ok, ok. Come back if you change your mind.


Ok, thanks


Okay, I'm ready to work!#0


Okay, I'm ready to work!#1


Okay, you can start!


Once it's done, you will be kicked from the server. Don't panic, as everything is fine.
Al termine dell'operazione sarai disconnesso dal server. Non temere, tutto andrà a buon fine.

Orange
Arancione

Pants


Parted
Divisi

Perky ponytail
Codino vivace

Peter


Pigtails
Codino

Pink
Rosa

Please change my race
Per favore cambia la mia razza

Please do, my dear
Certamente Caro, prosegui pure per favore

Please select a race
Per favore seleziona una razza

Pompadour
Pompadour

Punk
Punk

Purple


Rabbit ears


Read it


Red


Robe


Ronan


Rrrr pchhhh...


Scarf


Server
Server

ServiceNPC
NPC di servizio

Shop 1
Mercato 1

Shop 2
Mercato 2

Shop 3
Mercato 3

Shop 4
Mercato 4

Shop 5
Mercato 5

Shop 6
Mercato 6

Shop 7
Mercato 7

Short and curly
Corti e ricci

Short punk
Punk corti

Short robe


Short tank top


Shorts


Silver


Skirt


Sleeping Elf


So die already!


Sorcerer robe


Sorry you dont have components.
Sono spiacente ma non hai i componenti necessari.

Surprise me
Sorprendimi

T-neck sweater


Tank top


Teal
Tè Blu

TestNPC
TestNPC

There is also @@ on the docks, it will be a pleasure for her to help you!


They are really nice and may help you much more than me.


This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!


This note was left by somebody.


Tousled layers
Strati spettinati

Turtles drop it. Good luck!


V-neck sweater


Warin


Warning: All characters under this login will be changed.
Attenzione: tutti i personaggi all'interno di questo account verranno modificati.

Wave
Onda

Wavy
Ondulati

We are at [@@], it's a big port-city, many beginners start their adventure here.


Wedding
Sposalizio

Well, I'll see what happens. I hope that good luck will follow me around!


What can I dye for you today?


What color?


What do you want to do with it?


What do you want to dye?


What do you want today?


What do you want?
Che cosa vuoi?

What will you do during this break?


What? Never!


What? This reward is too small!


Where are we?


Where is this cup?..*hips*


White
Bianco

Why do you *hips* at me?!


Why i cant dye my item?


Why not, I need to train anyway.


Wild
Selvaggio

Witch hat


Would you be interested in a race change?
Sei interessato a cambiare razza?

Would you be interested in a sex change?
Ti interessa cambiare sesso?

Yeah, but what reward will I get?


Yeah, if you like, I'll open a market in this town, maybe you can visit me? It could be fun to see you again!


Yellow


Yes
Si

Yes, I can!


Yes, I did it!#0


Yes, I did it!#1


Yes, please!


Yo, I found it, cool! I have everything, except @@ @@. Can you bring it to me?


Yo, can you help me, bro?


Yo, can you help me, sis?


Yo, do you see what I mean?


Yo, look: my friend advised me to make a cool and tasty meal, and he gave me instructions.


Yo, now just bring it to me.


Yo, thank you!


Yo, that's good that you want to help me.


Yo, you really brought it!


You can start now.


You can't go there!


You don't have enough to pay for my services.
Non sei sufficientemente ricco per pagare i miei servizi.

You failed the task!#0


You failed the task!#1


You should go see the sailors around here.#0


You should go see the sailors around here.#1


Zzzzzzzzzz


is helping me.


