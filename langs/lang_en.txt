Copyright (C) 2010-2011  Evol Online
*hips*
*hips*

... By the power vested in me,
... By the power vested in me,

Ah, she *hips* is here.
Ah, she *hips* is here.

Are you tired of being what you are?
Are you tired of being what you are?

AreaNPC
AreaNPC

As you want!
As you want!

Ash brown
Ash brown

Bald
Bald

Bee spawned
Bee spawned

Billy Bons
Billy Bons

Black
Black

Blaf
Blaf

Ble *hips*... Imma great sail*hips*or!
Ble *hips*... Imma great sail*hips*or!

Blonde
Blonde

Blue
Blue

Boots
Boots

Bowl cut
Bowl cut

Brown
Brown

Brunette
Brunette

Bun
Bun

Bunny ears
Bunny ears

Bye bye!
Bye bye!

Can you do something with my color?
Can you do something with my color?

Cap
Cap

Center parting/Short and slick
Center parting/Short and slick

Choppy
Choppy

Combed back
Combed back

Continue?
Continue?

Copper
Copper

Copper red
Copper red

Could I ask you which language do you talk? Like that I add you on the list.
Could I ask you which language do you talk? Like that I add you on the list.

Demon
Demon

Desert hat
Desert hat

Devis
Devis

Did you bring me @@ @@?#0
Did you bring me @@ @@?

Did you bring me @@ @@?#1
Did you bring me @@ @@?

Do you know some interesting people around here?
Do you know some interesting people around here?

Do you want to create a bee?
Do you want to create a bee?

Don't worry, I'm sure that everything will turn out well for the both of us!
Don't worry, I'm sure that everything will turn out well for the both of us!

Don't you see that I'm already talking?
Don't you see that I'm already talking?

Done.
Done.

Dyeing failed. You lost dye item.
Dyeing failed. You lost dye item.

Dyer
Dyer

Elmo
Elmo

Emo
Emo

Enora
Enora

Eurni
Eurni

Eurni the Surgeon
Eurni the Surgeon

Flat ponytail
Flat ponytail

For dye your item, you need white item and dye with required color.
For dye your item, you need white item and dye with required color.

Fushia
Fushia

Ggrmm grmmm..
Ggrmm grmmm..

Gloves
Gloves

Good job!
Good job!

Green
Green

Hahaha, I won't take much of your time, go ahead!
Hahaha, I won't take much of your time, go ahead!

He takes a small list and starts reading it.
He takes a small list and starts reading it.

Headband
Headband

Hehe. Me, I will try to open a market, I've heard that it is a big city of commerce here, because of the port and all...
Hehe. Me, I will try to open a market, I've heard that it is a big city of commerce here, because of the port and all...

Hello @@
Hello @@

Hello dear, what do you want today?#0
Hello dear, what do you want today?

Hello dear, what do you want today?#1
Hello dear, what do you want today?

Hello, girl!
Hello, girl!

Hello, stranger!
Hello, stranger!

Here is your reward!
Here is your reward!

Hey! Yes, I'm already awake. So, it's end of our little road together, what will you do you now?
Hey! Yes, I'm already awake. So, it's end of our little road together, what will you do you now?

Hey! You saw? we've arrived on the port today, it was a long way from the other island...#0
Hey! You saw? we've arrived on the port today, it was a long way from the other island...

Hey! You saw? we've arrived to the port today, it was a long way from the other island...#1
Hey! You saw? we've arrived to the port today, it was a long way from the other island...

Hey, girl!
Hey, girl!

Hey, guy!
Hey, guy!

Hey, look at Julia, she's a beauty, isn't she? She's been looking at me for some minutes now... haha! Good luck, my friend.
Hey, look at Julia, she's a beauty, isn't she? She's been looking at me for some minutes now... haha! Good luck, my friend.

Hey, you lied to me.
Hey, you lied to me.

Hmm, I'm fine for now, thank you.
Hmm, I'm fine for now, thank you.

How can I get it
How can I get it

I can!
I can!

I don't need your help right now, come back later.
I don't need your help right now, come back later.

I don't want change my language, sorry.
I don't want change my language, sorry.

I gave you a reward. I can't give you more.
I gave you a reward. I can't give you more.

I made a mistake, I would like to change my language.#0
I made a mistake, I would like to change my language.

I made a mistake, I would like to change my language.#1
I made a mistake, I would like to change my language.

I need help for cleaning the wedge of the ship, but you aren't strong enough to help me.
I need help for cleaning the wedge of the ship, but you aren't strong enough to help me.

I need somebody who can clean the bottom of the ship, can you help me?
I need somebody who can clean the bottom of the ship, can you help me?

I sentence you to death... twice.
I sentence you to death... twice.

I speak Dutch
I speak Dutch

I speak English
I speak English

I speak Flemish
I speak Flemish

I speak French
I speak French

I speak Greek
I speak Greek

I speak Indonesian
I speak Indonesian

I speak Italian
I speak Italian

I speak Russian
I speak Russian

I speak Spanish
I speak Spanish

I think that I will explore around a bit and see what I can find, and you?
I think that I will explore around a bit and see what I can find, and you?

I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.
I think that the ship will still be here for some time, we need a lot of food and our ship needs some reparation before going back to the sea.

I think you can help me, right?
I think you can help me, right?

I want to be a Human
I want to be a Human

I want to be a Ifitron
I want to be a Ifitron

I want to be a Orc
I want to be a Orc

I want to be a Sparron
I want to be a Sparron

I want to be a Ukar
I want to be a Ukar

I want to dye to different color
I want to dye to different color

I will give you @@gp.
I will give you @@gp.

I will need some coffee...
I will need some coffee...

I will rest a bit here or in the town, I don't feel so well after this trip...
I will rest a bit here or in the town, I don't feel so well after this trip...

I will!
I will!

I wonder about that, I think I'll discover bit by bit and see what happens!#0
I wonder about that, I think I'll discover bit by bit and see what happens!

I wonder about that, I think I'll discover bit by bit and see what happens!#1
I wonder about that, I think I'll discover bit by bit and see what happens!

I'd like to get a different style.
I'd like to get a different style.

I'll come visit! I hope that this island will give us what we're looking for...
I'll come visit! I hope that this island will give us what we're looking for...

I'll get it!#0
I'll get it!

I'll get it!#1
I'll get it!

I'm Julia, my job is to register every body which enter or exit the ship.
I'm Julia, my job is to register every body which enter or exit the ship.

I'm busy#0
I'm busy

I'm busy#1
I'm busy

I've a girlfriend, and she needs me to do something unavailable.
I've a girlfriend, and she needs me to do something unavailable.

If you dont have it, then you cant dye.
If you dont have it, then you cant dye.

Imperial
Imperial

Infinitely long
Infinitely long

It's *hips* wrong, Imma not *hips* drunken.
It's *hips* wrong, Imma not *hips* drunken.

Julia
Julia

Khaki
Khaki

Kota
Kota

Leave alone my family treasure!
Leave alone my family treasure!

Leave it
Leave it

Let me see your work...
Let me see your work...

Let me sleep...
Let me sleep...

Let me think...
Let me think...

Lettuce
Lettuce

Lime
Lime

Lloyd
Lloyd

Long and curly
Long and curly

Long and slick
Long and slick

Long ponytail
Long ponytail

Make less noise, please.
Make less noise, please.

Mane
Mane

Marko
Marko

Mauve
Mauve

Maybe you will be the next?#0
Maybe you will be the next?

Maybe you will be the next?#1
Maybe you will be the next?

Mini-skirt
Mini-skirt

Mohawk
Mohawk

Move along, kid.
Move along, kid.

Navy blue
Navy blue

No
No

No problem, do you need something else?
No problem, do you need something else?

No!
No!

No, thank you
No, thank you

No, thanks!
No, thanks!

No, they are way too dangerous for me!
No, they are way too dangerous for me!

Not yet.
Not yet.

Note
Note

Nothing, sorry
Nothing, sorry

Now i have class
Now i have class

Of course! Tell me which language you speak.
Of course! Tell me which language you speak.

Off black
Off black

Oh really? I don't have much time right now, I need to go. Sorry.
Oh really? I don't have much time right now, I need to go. Sorry.

Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!
Oh, no problem! Anyway, if you have questions, you should see Julia at the reception, she's very nice!

Oh, thank you! Yo, what would I do without your help. Ok, see you!
Oh, thank you! Yo, what would I do without your help. Ok, see you!

Ok, Done. You need something else?
Ok, Done. You need something else?

Ok, ok. Come back if you change your mind.
Ok, ok. Come back if you change your mind.

Ok, thanks
Ok, thanks

Okay, I'm ready to work!#0
Okay, I'm ready to work!

Okay, I'm ready to work!#1
Okay, I'm ready to work!

Okay, you can start!
Okay, you can start!

Once it's done, you will be kicked from the server. Don't panic, as everything is fine.
Once it's done, you will be kicked from the server. Don't panic, as everything is fine.

Orange
Orange

Pants
Pants

Parted
Parted

Perky ponytail
Perky ponytail

Peter
Peter

Pigtails
Pigtails

Pink
Pink

Please change my race
Please change my race

Please do, my dear
Please do, my dear

Please select a race
Please select a race

Pompadour
Pompadour

Punk
Punk

Purple
Purple

Rabbit ears
Rabbit ears

Read it
Read it

Red
Red

Robe
Robe

Ronan
Ronan

Rrrr pchhhh...
Rrrr pchhhh...

Scarf
Scarf

Server
Server

ServiceNPC
ServiceNPC

Shop 1
Shop 1

Shop 2
Shop 2

Shop 3
Shop 3

Shop 4
Shop 4

Shop 5
Shop 5

Shop 6
Shop 6

Shop 7
Shop 7

Short and curly
Short and curly

Short punk
Short punk

Short robe
Short robe

Short tank top
Short tank top

Shorts
Shorts

Silver
Silver

Skirt
Skirt

Sleeping Elf
Sleeping Elf

So die already!
So die already!

Sorcerer robe
Sorcerer robe

Sorry you dont have components.
Sorry you dont have components.

Surprise me
Surprise me

T-neck sweater
T-neck sweater

Tank top
Tank top

Teal
Teal

TestNPC
TestNPC

There is also @@ on the docks, it will be a pleasure for her to help you!
There is also @@ on the docks, it will be a pleasure for her to help you!

They are really nice and may help you much more than me.
They are really nice and may help you much more than me.

This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!
This island is kinda funny for starting an adventure, and it's a good place for meeting people too, haha!

This note was left by somebody.
This note was left by somebody.

Tousled layers
Tousled layers

Turtles drop it. Good luck!
Turtles drop it. Good luck!

V-neck sweater
V-neck sweater

Warin
Warin

Warning: All characters under this login will be changed.
Warning: All characters under this login will be changed.

Wave
Wave

Wavy
Wavy

We are at [@@], it's a big port-city, many beginners start their adventure here.
We are at [@@], it's a big port-city, many beginners start their adventure here.

Wedding
Wedding

Well, I'll see what happens. I hope that good luck will follow me around!
Well, I'll see what happens. I hope that good luck will follow me around!

What can I dye for you today?
What can I dye for you today?

What color?
What color?

What do you want to do with it?
What do you want to do with it?

What do you want to dye?
What do you want to dye?

What do you want today?
What do you want today?

What do you want?
What do you want?

What will you do during this break?
What will you do during this break?

What? Never!
What? Never!

What? This reward is too small!
What? This reward is too small!

Where are we?
Where are we?

Where is this cup?..*hips*
Where is this cup?..*hips*

White
White

Why do you *hips* at me?!
Why do you *hips* at me?!

Why i cant dye my item?
Why i cant dye my item?

Why not, I need to train anyway.
Why not, I need to train anyway.

Wild
Wild

Witch hat
Witch hat

Would you be interested in a race change?
Would you be interested in a race change?

Would you be interested in a sex change?
Would you be interested in a sex change?

Yeah, but what reward will I get?
Yeah, but what reward will I get?

Yeah, if you like, I'll open a market in this town, maybe you can visit me? It could be fun to see you again!
Yeah, if you like, I'll open a market in this town, maybe you can visit me? It could be fun to see you again!

Yellow
Yellow

Yes
Yes

Yes, I can!
Yes, I can!

Yes, I did it!#0
Yes, I did it!

Yes, I did it!#1
Yes, I did it!

Yes, please!
Yes, please!

Yo, I found it, cool! I have everything, except @@ @@. Can you bring it to me?
Yo, I found it, cool! I have everything, except @@ @@. Can you bring it to me?

Yo, can you help me, bro?
Yo, can you help me, bro?

Yo, can you help me, sis?
Yo, can you help me, sis?

Yo, do you see what I mean?
Yo, do you see what I mean?

Yo, look: my friend advised me to make a cool and tasty meal, and he gave me instructions.
Yo, look: my friend advised me to make a cool and tasty meal, and he gave me instructions.

Yo, now just bring it to me.
Yo, now just bring it to me.

Yo, thank you!
Yo, thank you!

Yo, that's good that you want to help me.
Yo, that's good that you want to help me.

Yo, you really brought it!
Yo, you really brought it!

You can start now.
You can start now.

You can't go there!
You can't go there!

You don't have enough to pay for my services.
You don't have enough to pay for my services.

You failed the task!#0
You failed the task!

You failed the task!#1
You failed the task!

You should go see the sailors around here.#0
You should go see the sailors around here.

You should go see the sailors around here.#1
You should go see the sailors around here.

Zzzzzzzzzz
Zzzzzzzzzz

is helping me.
is helping me.

